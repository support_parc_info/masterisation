# Définissez vos paramètres
$SSID = 'My_int'
$Password = 'QAdUMLWZ7PE$$_2020'

# Créez un profil Wi-Fi
$profileXml = @"
<?xml version="1.0"?>
<WLANProfile xmlns="http://www.microsoft.com/networking/WLAN/profile/v1">
    <name>$SSID</name>
    <SSIDConfig>
        <SSID>
            <hex>{0}</hex>
            <name>$SSID</name>
        </SSID>
    </SSIDConfig>
    <connectionType>ESS</connectionType>
    <connectionMode>auto</connectionMode>
    <MSM>
        <security>
            <authEncryption>
                <authentication>WPA2PSK</authentication>
                <encryption>AES</encryption>
                <useOneX>false</useOneX>
            </authEncryption>
            <sharedKey>
                <keyType>passPhrase</keyType>
                <protected>false</protected>
                <keyMaterial>$Password</keyMaterial>
            </sharedKey>
        </security>
    </MSM>
</WLANProfile>
"@ -f [BitConverter]::ToString([Text.Encoding]::ASCII.GetBytes($SSID)) -replace '-'

# Ajoutez le profil Wi-Fi
$profilePath = "$env:TEMP\$SSID.xml"
$profileXml | Out-File $profilePath
netsh wlan add profile filename=$profilePath

# Supprimez le fichier XML temporaire
Remove-Item $profilePath

# Connectez-vous au réseau Wi-Fi
netsh wlan connect name=$SSID

# Pause de 10 secondes
Start-Sleep -Seconds 10

# Installation de Chocolatey et de quelques logiciels
$chocoInstallScript = {
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    iex ((Invoke-WebRequest -Uri https://chocolatey.org/install.ps1).Content)
    
    choco install googlechrome --version 117.0.5938.150 -y
    choco install 7zip.install --version 23.1.0 -y
    choco install foxitreader --version 2023.2.0.21408 -y
    choco install office365business --version 16731.20290 -y
    choco install slack --version 4.34.119 -y
    choco install microsoft-teams.install --version 1.6.0.24078 -y
}
& $chocoInstallScript

# Configuration régionale et linguistique
Set-Culture -CultureInfo fr-FR
Set-WinSystemLocale -SystemLocale fr-FR
Set-WinHomeLocation -GeoId 84  # Le GeoId 84 correspond à la France
Set-WinSystemLocale fr-FR

$FrenchLanguage = New-WinUserLanguageList fr-FR
$FrenchLanguage[0].InputMethodTips.Add("040c:0000040c")
Set-WinUserLanguageList $FrenchLanguage -Force
Set-WinDefaultInputMethodOverride "040c:0000040c"

$regPath = "HKCU:\Control Panel\International"
Set-ItemProperty -Path $regPath -Name "LocaleName" -Value "fr-FR"
Set-ItemProperty -Path $regPath -Name "sShortDate" -Value "dd/MM/yyyy"
Set-ItemProperty -Path $regPath -Name "sLongDate" -Value "dddd, d MMMM yyyy"
Set-ItemProperty -Path $regPath -Name "sTimeFormat" -Value "HH:mm:ss"
Set-ItemProperty -Path $regPath -Name "sDecimal" -Value ","
Set-ItemProperty -Path $regPath -Name "sThousand" -Value " "

Stop-Process -Name explorer -Force
Start-Process explorer

# Renommage de la machine
function Set-ComputerName {
    $Var1 = "P-W10D-"
    $SerialNumber = (wmic bios get Serialnumber).Split([char]::NewLine)[1].Trim()
    $Name = $Var1 + $SerialNumber
    Rename-Computer -NewName $Name.Trim()
    $Name
}

Set-ComputerName
