# Script pour automatiser la creation d'utilisateur
#Install-Module -Name AzureAD
#Install-Module MSOnline




Connect-AzureAD
Write-Host "                                                                                                                                                                                                   
@@@@%               &@@@@        
@@@@@             @@@@@         
@  @@@@@         @@@@@  @       
@@  @@@@@       @@@@@  @@       
@@@  @@@@@     @@@@@  @@@       
@@@@  @@@@@   @@@@@  @@@@       
@@@@@  @@@@@ @@@@&  @@@@@       
@@@@@   @@@@@@@@&   @@@@@       
@@@@@    @@@@@@@    @@@@@       
@@@@@     @@@@@     @@@@@       
@@@@@     @@@@@     @@@@@       
@@@@@     @@@@@     @@@@@


"


    Write-Host " ***************************************"
    Write-Host " *  Gestionnaire d'utilisateur Azure   *"
    Write-Host " ***************************************"
    Write-Host " "
    Write-Host " Choisissez une option"
    Write-Host
    Write-Host " 1.) Creer un utilisateur"
    Write-Host " 2.) Modifier un utilisateur"
    Write-Host " 3.) Desactiver un utilsateur"
    Write-Host " 4.) Supprimer un utilisateur"
    $Choix = Read-Host -Prompt " Choisissez une option : 1-4        Quitter :Q  -Forgeground   Help : H"

if ($Choix -eq "1") { 

    #CREATION DU COMPTE
    $nom = Read-Host -Prompt "Entrez votre Nom"
    $prenom = Read-Host -Prompt "Entrez votre prenom"
    $fonction = Read-Host -Prompt "Entrez votre fonction (poste)"
    $equipe = Read-Host -Prompt "Entrez le service"
    $Pswd = Read-Host "Entrez votre mot de passe" -AsSecureString
    $^ = $prenom.Substring(0,1)
    $upn = "$^.$nom@myunisoft.fr".ToLower()
    
    Add-Type -Path 'C:\Program Files\WindowsPowerShell\Modules\AzureAD\2.0.2.140\Microsoft.Open.AzureAD16.Graph.Client.dll'
    $PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
    $PasswordProfile.Password = $Pswd
    
    #Création de l'utilisateur et parametrage du mot de passe
    New-AzureADUser -DisplayName "$prenom $nom" -UserPrincipalName "$upn" -PasswordProfile $PasswordProfile -mailNickname "$^.$nom".ToLower() -GivenName "$prenom" -PostalCode "91300" -State "Essonne" -StreetAddress "4 Rue Galvani" -Surname "$nom".ToUpper()  -City "MASSY" -CompanyName "MY UNISOFT SAS" -Country "Ile-de-France" -Department "$equipe" -JobTitle "$fonction"  -AccountEnabled $true
    $userId = (Get-AzureAdUser -ObjectId "$upn").ObjectId
    Set-AzureADUserPassword -ObjectId  "$userId" -Password $Pswd
    Start-Sleep -Seconds 10
    Add-AzureADGroupMember "$UserId" -RefObjectId "9594780d-bf57-46ee-a4cc-5bed44aff10b"


    #MODIFICATION D'UTILISATEUR
    <#
    else if ($Choix -eq "2")
    {
        Write-Host"En cours de developpement..."
    }


    #DESACTIVATION D'UTILISATEUR
    else if ($Choix -eq "3")
    {
        Write-Host -Prompt "Tu crois que c'est du respect mon garçon ?"
    }


    #SUPRESSION D'UTILISATEUR
    else if ($Choix -eq "4")
    {
        Write-Host -Prompt "Tu crois que c'est du respect mon garçon ?"
    }
    #>


}



#LICENCES
<
Connect-MsolService
Start-Sleep -Seconds 10
Get-MsolUser -UserPrincipalName $upn
Set-MsolUser -UserPrincipalName $upn -UsageLocation US
Set-MsolUserLicense -UserPrincipalName $upn -AddLicenses "reseller-account:SPB"


Read-Host -Prompt "Appuyez sur entrer pour fermer"
#>