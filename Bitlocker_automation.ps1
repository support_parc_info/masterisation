﻿# Suppression de la clé FVE dans le registre
$registryPath = "HKLM:\SOFTWARE\Policies\Microsoft\FVE"
if (Test-Path $registryPath) {
    Remove-Item -Path $registryPath -Recurse -Force
    Write-Host "Clé FVE supprimée avec succès"
} else {
    Write-Host "La clé FVE n'a pas été trouvée"
}

# Activation de la connexion par code PIN d'usage via GPO
Import-Module GroupPolicy

$gpoName = "Activer la connexion par le code PIN d'usage"
New-GPO -Name $gpoName -Comment "GPO pour activer la connexion par code PIN d'usage" -WhatIf

# Modifier le GPO pour activer la politique
$policyName = "Turn on PIN sign-in"
Set-GPRegistryValue -Name $gpoName -Key "HKLM\Software\Policies\Microsoft\Windows\System" -ValueName $policyName -Type DWord -Value 1

Write-Host "Stratégie de groupe modifiée avec succès"

# Configure BitLocker pour chiffrer uniquement l'espace utilisé
manage-bde -usedspaceonly c:

# Définir le mode de chiffrement pour utiliser l'algorithme moderne (par exemple, XTS-AES 256)
manage-bde -setencryptionmethod c: XTS_AES256

# Démarrer le chiffrement
manage-bde -on c:

# Délai d'attente pour permettre à BitLocker de commencer le chiffrement
Start-Sleep -Seconds 30

# Conversion de base36 en base10 pour obtenir le PIN
function convertFrom-base36 {
    [CmdletBinding()]
    param ([parameter(valuefrompipeline=$true, HelpMessage="Alphadecimal string to convert")][string]$base36Num)
    
    # Si $base36Num est vide, obtenir le numéro de série du BIOS
    if (-not $base36Num) {
        $serialNumber = (wmic bios get serialnumber | Where-Object { $_ -match '\S' } | Select-Object -Skip 1).Trim()
        $base36Num = $serialNumber
    }

    $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"
    $inputarray = $base36Num.tolower().tochararray()
    [array]::reverse($inputarray)
    [long]$decNum = 0
    $pos = 0

    foreach ($c in $inputarray) {
        $decNum += $alphabet.IndexOf($c) * [long][Math]::Pow(36, $pos)
        $pos++
    }

    # Récupère seulement les 6 premiers chiffres du résultat
    return ($decNum.ToString()).Substring(0, 6)
}

# Obtention du PIN et exécution de la commande manage-bde pour changer le PIN
$pin = convertFrom-base36
manage-bde -protectors -add c: -TPMAndPIN $pin

Write-Host "Protection BitLocker activée avec succès avec le PIN: $pin"